# Rich text editor for Vuetify 2.0

## Installing

```text
npm i -S rich-text-editor-vuetify
```

## Getting started

```javascript
// in main.js / index.js

import Vue from 'vue'
import RichTextEditor from 'rich-text-editor-vuetify'

[...]

Vue.use(RichTextEditor)
```

## Example

```javascript
<template>
  <rich-text-editor @update="onUpdate"/>
</template>

<script>
export default {
  name: 'RichTextEditPanel',
  data () {
    return {
      text: ''
    }
  },
  methods: {
    onUpdate (text) {
      this.text = text
    }
  }
}
</script>
```
