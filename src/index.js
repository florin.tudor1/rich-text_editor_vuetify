import _viewsEditor from './views/editor'

export default {
  /**
   */
  install (Vue) {
    Vue.component('rich-text-editor', _viewsEditor)
  }
}
